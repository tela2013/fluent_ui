import 'package:fluent_ui/fluent_ui.dart';

//色块展示页面
class ColorsPage extends StatelessWidget {
  const ColorsPage({Key? key, this.controller}) : super(key: key);

  final ScrollController? controller;

  // 一个色块的布局
  Widget buildColorBlock(String name, Color color) {
    return Container(
      constraints: const BoxConstraints(
        minHeight: 65,
        minWidth: 65,
      ),
      padding: const EdgeInsets.all(2.0),
      color: color,
      //色块上插入列布局,在底部显示文字
      child: Column(mainAxisAlignment: MainAxisAlignment.end,
          children: [
        // 设置文字样式基于背景亮度来变化
        Text(name, style: TextStyle(color: color.basedOnLuminance())),
      ]),
    );
  }

  @override
  Widget build(BuildContext context) {
    //定义一个分隔线
    const Divider divider = Divider(
      style: DividerThemeData(
        verticalMargin: EdgeInsets.all(10),
        horizontalMargin: EdgeInsets.all(10),
      ),
    );
    // 使用带导航面板的框架页
    return ScaffoldPage(
      header: const PageHeader(title: Text('Colors Showcase')),
      content: ListView(
        padding: EdgeInsets.only(
          bottom: kPageDefaultVerticalPadding,
          left: PageHeader.horizontalPadding(context),
          right: PageHeader.horizontalPadding(context),
        ),
        controller: controller,
        children: [
          //带标签名称的控件
          InfoLabel(
            label: 'Primary Colors',
            child: Wrap(
              spacing: 10,
              runSpacing: 10,
              children: Colors.accentColors.map<Widget>((color) {
                return buildColorBlock('', color);
              }).toList(),
            ),
          ),
          //插入分隔线
          divider,
          InfoLabel(
            label: 'Info Colors',
            child: Wrap(
              spacing: 10,
              runSpacing: 10,
              children: [
                buildColorBlock('Warning 1', Colors.warningPrimaryColor),
                buildColorBlock('Warning 2', Colors.warningSecondaryColor),
                buildColorBlock('Error 1', Colors.errorPrimaryColor),
                buildColorBlock('Error 2', Colors.errorSecondaryColor),
                buildColorBlock('Success 1', Colors.successPrimaryColor),
                buildColorBlock(
                    'Success 2', Colors.successSecondaryColor.toAccentColor()),
              ],
            ),
          ),
          divider,
          InfoLabel(
            label: 'All Shades',
            child:
                //插入列布局
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              //    插入行布局
              Row(children: [
                buildColorBlock('Black', Colors.black),
                buildColorBlock('White', Colors.white),
              ]),
              //    插入行间隔
              const SizedBox(height: 10),
              //  插入黑白灰的色块,自动换行
              Wrap(
                children: List.generate(22, (index) {
                  return buildColorBlock(
                    'Grey#${(index + 1) * 10}',
                    Colors.grey[(index + 1) * 10],
                  );
                }),
              ),
              const SizedBox(height: 50),
              //插入其它彩色块的布局
              Wrap(
                children: accent,
                runSpacing: 10,
                spacing: 10,
              ),
            ]),
          ),
        ],
      ),
    );
  }

  List<Widget> get accent {
    List<Widget> children = [];
    for (final accent in Colors.accentColors) {
      children.add(
        Wrap(
          // mainAxisSize: MainAxisSize.min,
          children: List.generate(accent.swatch.length, (index) {
            final name = accent.swatch.keys.toList()[index];
            final color = accent.swatch[name];
            return buildColorBlock(name, color!);
          }),
        ),
      );
    }
    return children;
  }
}
