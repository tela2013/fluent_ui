import 'package:fluent_ui/fluent_ui.dart';

import 'settings.dart';
//文章排版页
class TypographyPage extends StatefulWidget {
  const TypographyPage({Key? key}) : super(key: key);

  @override
  _TypographyPageState createState() => _TypographyPageState();
}

class _TypographyPageState extends State<TypographyPage> {
  Color? color;
  //设置缩放比例
  double scale = 1.0;

  //一个小颜色块的布局
  Widget buildColorBox(Color color) {
    const double boxSize = 25.0;
    return Container(
      height: boxSize,
      width: boxSize,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(4.0),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasFluentTheme(context));
    Typography typography = FluentTheme.of(context).typography;
    color ??= typography.display!.color;
    typography = typography.apply(displayColor: color!);
    const Widget spacer = SizedBox(height: 4.0);
    //使用框架页
    return ScaffoldPage(
      header: PageHeader(
        //设置页面标题
        title: const Text('Typography showcase'),
        //设置页面的工具条
        commandBar: SizedBox(
          //设置工具条的宽度
          width: 280.0,
          //设置工具条的子元素是一个提示文字
          child: Tooltip(
            message: 'Pick a text color',
            //设置下拉框
            child: Combobox<Color>(
              placeholder: const Text('Text Color'),
              onChanged: (c) => setState(() => color = c),
              value: color,
              items: [
                //设置下拉框选项
                ComboboxItem(
                  //插入行布局
                  child: Row(children: [
                    //左边是颜色块
                    buildColorBox(Colors.white),
                    //中间是间距
                    const SizedBox(width: 10.0),
                    //右侧是文字
                    const Text('White'),
                  ]),
                  value: Colors.white,
                ),
                ComboboxItem(
                  child: Row(children: [
                    buildColorBox(Colors.black),
                    const SizedBox(width: 10.0),
                    const Text('Black'),
                  ]),
                  value: Colors.black,
                ),
                ...List.generate(Colors.accentColors.length, (index) {
                  final color = Colors.accentColors[index];
                  return ComboboxItem(
                    child: Row(children: [
                      buildColorBox(color),
                      const SizedBox(width: 10.0),
                      Text(accentColorNames[index + 1]),
                    ]),
                    value: color,
                  );
                }),
              ],
            ),
          ),
        ),
      ),
      //设置页面的主要内容
      content: Padding(
        //设置页边距
        padding: EdgeInsets.symmetric(
          horizontal: PageHeader.horizontalPadding(context),
        ),
        //设置行布局为左对齐
        child: Row(crossAxisAlignment: CrossAxisAlignment.start,
            children: [
          //  设置第一个元素为弹性布局
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                //插入一根分隔线
                const Divider(
                  style: DividerThemeData(horizontalMargin: EdgeInsets.zero),
                ),
                Expanded(
                  child: ListView(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisSize: MainAxisSize.min,
                    children: [
                      Text('Display',
                          //文字样式使用系统自带的文章样式,再加上缩放比例
                          style:
                              typography.display?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Title Large',
                          style: typography.titleLarge
                              ?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Title',
                          style:
                              typography.title?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Subtitle',
                          style: typography.subtitle
                              ?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Body Large',
                          style: typography.bodyLarge
                              ?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Body Strong',
                          style: typography.bodyStrong
                              ?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Body',
                          style: typography.body?.apply(fontSizeFactor: scale)),
                      spacer,
                      Text('Caption',
                          style:
                              typography.caption?.apply(fontSizeFactor: scale)),
                      spacer,
                    ],
                  ),
                ),
              ],
            ),
          ),
          //  设置第二个元素
          Semantics(
            label: 'Scale',
            //插入一个滑动控件
            child: Slider(
              vertical: true,
              value: scale,
              onChanged: (v) => setState(() => scale = v),
              label: scale.toStringAsFixed(2),
              max: 2,
              min: 0.5,
              // style: SliderThemeData(useThumbBall: false),
            ),
          ),
        ]),
      ),
    );
  }
}
