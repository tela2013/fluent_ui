// ignore_for_file: avoid_print

import 'package:fluent_ui/fluent_ui.dart';
//提示面板进度条标签页的布局
class Others extends StatefulWidget {
  const Others({Key? key}) : super(key: key);

  @override
  _OthersState createState() => _OthersState();
}

class _OthersState extends State<Others> {
  final otherController = ScrollController();

  int currentIndex = 0;

  final flyoutController = FlyoutController();

  bool checked = false;

  @override
  void dispose() {
    flyoutController.dispose();
    otherController.dispose();
    super.dispose();
  }

  DateTime date = DateTime.now();

  late List<Tab> tabs;

  @override
  void initState() {
    super.initState();
    //创建3个标签页的列表
    tabs = List.generate(3, (index) {
      late Tab tab;
      tab = Tab(
        text: Text('$index'),
        onClosed: () {
          _handleTabClosed(tab);
        },
      );
      return tab;
    });
  }

  @override
  Widget build(BuildContext context) {
    //使用框架页
    return ScaffoldPage(
      //设置页面标题
      header: const PageHeader(title: Text('Others')),
      //插入列表布局
      content: ListView(
        //设置边距
        padding: EdgeInsets.only(
          bottom: kPageDefaultVerticalPadding,
          left: PageHeader.horizontalPadding(context),
          right: PageHeader.horizontalPadding(context),
        ),
        //设置滚动条控制器
        controller: otherController,
        children: [
          //根据系统的信息条枚举数量,创建一组控件
          ...List.generate(InfoBarSeverity.values.length, (index) {
            //模拟信息的严重程度
            final severity = InfoBarSeverity.values[index];
            final titles = [
              'Long title~',
              'Short title!',
            ];
            final descs = [
              'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book',
              'Short desc',
            ];
            // 信息条真正的布局
            return Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: InfoBar(
                title: Text(titles[index.isEven ? 0 : 1]),
                content: Text(descs[index.isEven ? 0 : 1]),
                isLong: InfoBarSeverity.values.indexOf(severity).isEven,
                //设置严重程度的等级
                severity: severity,
                //设置菜单按钮
                action: () {
                  //如果是第一个
                  if (index == 0) {
                    //按钮提示文字
                    return Tooltip(
                      message: 'This is a tooltip',
                      //菜单按钮
                      child: Button(
                        child: const Text('Hover this button to see a tooltip'),
                        onPressed: () {
                          //按钮点击动作
                          print('pressed button with tooltip');
                        },
                      ),
                    );
                  } else {
                    //如果是第四个
                    if (index == 3) {
                      //插入一个带透明度的信息提示框
                      return Flyout(
                        controller: flyoutController,
                        contentWidth: 450,
                        content: const FlyoutContent(
                          child: Text(
                              'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.'),
                        ),
                        //设置菜单按钮
                        child: Button(
                          child: const Text('Open flyout'),
                          onPressed: () {
                            //按钮点击动作是打开信息提示框
                            flyoutController.open = true;
                          },
                        ),
                      );
                    }
                  }
                }(),
                //设置关闭信息提示框的动作
                onClose: () {
                  print('closed');
                },
              ),
            );
          }),
          Wrap(children: [
            //插入一个不能点击的列表项
            const ListTile(
              title: Text('ListTile Title'),
              subtitle: Text('ListTile Subtitle'),
            ),
            //插入一个带点击动作的列表视图
            TappableListTile(
              leading: const CircleAvatar(),
              title: const Text('TappableListTile Title'),
              subtitle: const Text('TappableListTile Subtitle'),
              onTap: () {
                print('tapped tappable list tile');
              },
            ),
          ]),
          //插入行布局
          Row(children: const [
            Padding(
              padding: EdgeInsets.all(6),
              //插入一个进度条
              child: ProgressBar(value: 50),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              //插入一个进度环
              child: ProgressRing(value: 85),
            ),
            Padding(
              padding: EdgeInsets.all(6),
              //插入一个进度条,没有具体进度值,会无限循环
              child: ProgressBar(),
            ),
            Padding(
              padding: EdgeInsets.all(10),
              //插入一个进度环,没有具体进度值,会无限循环
              child: ProgressRing(),
            ),
          ]),
          // Row(children: [
          //   CalendarView(
          //     onDateChanged: _handleDateChanged,
          //     firstDate: DateTime.now().subtract(Duration(days: 365 * 100)),
          //     lastDate: DateTime.now().add(Duration(days: 365 * 100)),
          //     initialDate: date,
          //     currentDate: date,
          //     onDisplayedMonthChanged: (date) {
          //       setState(() => this.date = date);
          //     },
          //   ),
          //   CalendarView(
          //     onDateChanged: _handleDateChanged,
          //     firstDate: DateTime.now().subtract(Duration(days: 365 * 100)),
          //     lastDate: DateTime.now().add(Duration(days: 365 * 100)),
          //     initialDate: date,
          //     currentDate: date,
          //     onDisplayedMonthChanged: (date) {
          //       setState(() => this.date = date);
          //     },
          //     initialCalendarMode: DatePickerMode.year,
          //   ),
          // ]),
          const SizedBox(height: 10),
          Container(
            height: 400,
            decoration: BoxDecoration(
              border: Border.all(
                  color: FluentTheme.of(context).accentColor, width: 1.0),
            ),
            //插入一个标签页控件
            child: TabView(
              currentIndex: currentIndex,
              onChanged: _handleTabChanged,
              //重新排序的动作
              onReorder: (oldIndex, newIndex) {
                setState(() {
                  if (oldIndex < newIndex) {
                    newIndex -= 1;
                  }
                  final Tab item = tabs.removeAt(oldIndex);
                  tabs.insert(newIndex, item);
                  if (currentIndex == newIndex) {
                    currentIndex = oldIndex;
                  } else if (currentIndex == oldIndex) {
                    currentIndex = newIndex;
                  }
                });
              },
              //新建标签页的动作
              onNewPressed: () {
                setState(() {
                  late Tab tab;
                  tab = Tab(
                    text: Text('${tabs.length}'),
                    onClosed: () {
                      _handleTabClosed(tab);
                    },
                  );
                  tabs.add(tab);
                });
              },
              //绑定标签页列表
              tabs: tabs,
              //设置标签页的页面内容
              bodies: List.generate(
                tabs.length,
                (index) => Container(
                  color: Colors.accentColors[index.clamp(
                    0,
                    Colors.accentColors.length - 1,
                  )],
                  //使用图层布局
                  child: Stack(children: [
                    const Positioned.fill(child: FlutterLogo()),
                    Align(
                      alignment: Alignment.center,
                      child: SizedBox(
                        width: 250.0,
                        height: 200.0,
                        //插入毛玻璃视图控件
                        child: Acrylic(
                          child: Center(
                            child: Text(
                              'A C R Y L I C',
                              style:
                                  FluentTheme.of(context).typography.titleLarge,
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _handleTabChanged(int index) {
    setState(() => currentIndex = index);
  }

  void _handleTabClosed(Tab tab) {
    setState(() {
      tabs.remove(tab);
      if (currentIndex > tabs.length - 1) currentIndex--;
    });
  }

  // void _handleDateChanged(DateTime date) {

  // }

}
