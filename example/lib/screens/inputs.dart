// ignore_for_file: avoid_print

import 'package:fluent_ui/fluent_ui.dart';

const Widget spacer = SizedBox(height: 5.0);

// 表单控件页面
class InputsPage extends StatefulWidget {
  const InputsPage({Key? key}) : super(key: key);

  @override
  _InputsPageState createState() => _InputsPageState();
}

class _InputsPageState extends State<InputsPage> {
  // 控件禁用状态位
  bool disabled = false;

  bool value = false;

  double sliderValue = 5;
  double get max => 9;

  final FlyoutController controller = FlyoutController();
  final ScrollController scrollController = ScrollController();

  @override
  void dispose() {
    controller.dispose();
    scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //插入框架页面部件
    return ScaffoldPage(
      //设置页面名称
      header: PageHeader(
        title: const Text('Inputs showcase'),
        // 表单控件禁用或开启的滑动开关
        commandBar: ToggleSwitch(
          checked: disabled,
          onChanged: (v) => setState(() => disabled = v),
          content: const Text('Disabled'),
        ),
      ),
      //设置页面窗体内容
      content: SingleChildScrollView(
        //插入滚动条布局
        controller: scrollController,
        padding: EdgeInsets.symmetric(
          horizontal: PageHeader.horizontalPadding(context),
        ),
        child: SizedBox(
          width: double.infinity,
          //插入自动换行的流式布局,并设置窗体元素之间的间距
          child: Wrap(spacing: 10, runSpacing: 10,
              children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              //插入一个背景色面板
              child: Mica(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  //插入一个组件面板
                  child: InfoLabel(
                    label: 'Interactive Inputs',
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        //复选框
                        Checkbox(
                          checked: value,
                          onChanged: disabled
                              ? null
                              : (v) => setState(() => value = v ?? false),
                          content: Text(
                            'Checkbox ${value ? 'on ' : 'off'}',
                            style: TextStyle(
                              color: FluentTheme.of(context).inactiveColor,
                            ),
                          ),
                        ),
                        //开关部件
                        ToggleSwitch(
                          checked: value,
                          onChanged: disabled
                              ? null
                              : (v) => setState(() => value = v),
                          content: Text(
                            'Switcher ${value ? 'on ' : 'off'}',
                            style: TextStyle(
                              color: FluentTheme.of(context).inactiveColor,
                            ),
                          ),
                        ),
                        //单选框
                        RadioButton(
                          checked: value,
                          onChanged: disabled
                              ? null
                              : (v) => setState(() => value = v),
                          content: Text(
                            'Radio Button ${value ? 'on ' : 'off'}',
                            style: TextStyle(
                              color: FluentTheme.of(context).inactiveColor,
                            ),
                          ),
                        ),
                        spacer,
                        //按钮式交换开关
                        ToggleButton(
                          child: const Text('Toggle Button'),
                          checked: value,
                          onChanged: disabled
                              ? null
                              : (value) => setState(() => this.value = value),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            _buildButtons(),
            _buildSliders(),
            //第四个面板布局
            Mica(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                //插入一个可扩展菜单部件
                child: Flyout(
                  //设置收起的内容布局
                  content: Padding(
                    padding: const EdgeInsets.only(left: 27),
                    child: FlyoutContent(
                      padding: EdgeInsets.zero,
                      child: ListView(shrinkWrap: true, children: [
                        TappableListTile(
                            title: const Text('New'), onTap: () {}),
                        TappableListTile(
                            title: const Text('Open'), onTap: () {}),
                        TappableListTile(
                            title: const Text('Save'), onTap: () {}),
                        TappableListTile(
                            title: const Text('Exit'), onTap: () {}),
                      ]),
                    ),
                  ),
                  verticalOffset: 20,
                  contentWidth: 100,
                  controller: controller,
                  //设置展示的布局
                  child: Button(
                    child: const Text('File'),
                    onPressed: disabled ? null : () => controller.open = true,
                  ),
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
  //第二个面板控件
  Widget _buildButtons() {
    const double splitButtonHeight = 25.0;
    return Mica(
      child: Padding(
        padding: const EdgeInsets.all(8),
        child: InfoLabel(
          label: 'Buttons',
          child:
              //插入列视图
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            // 按钮部件
            Button(
              child: const Text('Show Dialog'),
              onPressed: disabled
                  ? null
                  : () {
                      //显示对话框
                      showDialog(
                        context: context,
                        builder: (_) => ContentDialog(
                          title: const Text('Delete file permanently?'),
                          content: const Text(
                            'If you delete this file, you won\'t be able to recover it. Do you want to delete it?',
                          ),
                          actions: [
                            Button(
                              child: const Text('Delete'),
                              onPressed: () {
                                // Delete file here
                              },
                            ),
                            Button(
                              child: const Text('Cancel'),
                              onPressed: () => Navigator.pop(context),
                            ),
                          ],
                        ),
                      );
                    },
            ),
            spacer,
            //图标按钮
            IconButton(
              icon: const Icon(FluentIcons.add),
              onPressed: disabled ? null : () => print('pressed icon button'),
            ),
            spacer,
            SizedBox(
              height: splitButtonHeight,
              //插入分栏按钮
              child: SplitButtonBar(buttons: [
                //左侧是按钮
                Button(
                  child: SizedBox(
                    height: splitButtonHeight,
                    child: Container(
                      decoration: BoxDecoration(
                        color: disabled
                            ? FluentTheme.of(context).accentColor.darker
                            : FluentTheme.of(context).accentColor,
                        borderRadius: const BorderRadius.horizontal(
                          left: Radius.circular(4.0),
                        ),
                      ),
                      height: 24,
                      width: 24,
                    ),
                  ),
                  onPressed: disabled ? null : () {},
                ),
                //右侧是图标按钮
                IconButton(
                  icon: const SizedBox(
                    height: splitButtonHeight,
                    child: Icon(FluentIcons.chevron_down, size: 10.0),
                  ),
                  onPressed: disabled ? null : () {},
                ),
              ]),
            ),
            spacer,
            //  文字型按钮
            TextButton(
              child: const Text('TEXT BUTTON'),
              onPressed: disabled
                  ? null
                  : () {
                      print('pressed text button');
                    },
            ),
            spacer,
            // 填充色按钮
            FilledButton(
              child: const Text('FILLED BUTTON'),
              onPressed: disabled
                  ? null
                  : () {
                      print('pressed filled button');
                    },
            ),
            spacer,
            // 轮廓按钮
            OutlinedButton(
              child: const Text('OUTLINED BUTTON'),
              onPressed: disabled
                  ? null
                  : () {
                      print('pressed outlined button');
                    },
            ),
          ]),
        ),
      ),
    );
  }
  // 第三个控件面板
  Widget _buildSliders() {
    return Mica(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: InfoLabel(
          label: 'Sliders',
          child:
              Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            Row(mainAxisSize: MainAxisSize.min, children: [
              Flexible(
                fit: FlexFit.loose,
                child: Column(children: [
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 8),
                    width: 200,
                    //插入一个横线型的滑动条
                    child: Slider(
                      max: max,
                      label: '${sliderValue.toInt()}',
                      value: sliderValue,
                      onChanged: disabled
                          ? null
                          : (v) => setState(() => sliderValue = v),
                      divisions: 10,
                    ),
                  ),
                  //插入一个星级评分部件
                  RatingBar(
                    amount: max.toInt(),
                    rating: sliderValue,
                    onChanged: disabled
                        ? null
                        : (v) => setState(() => sliderValue = v),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8),
                //插入一个竖线型的滑动条
                child: Slider(
                  vertical: true,
                  max: max,
                  label: '${sliderValue.toInt()}',
                  value: sliderValue,
                  onChanged:
                      disabled ? null : (v) => setState(() => sliderValue = v),
                  // style: SliderThemeData(useThumbBall: false),
                ),
              ),
            ]),
          ]),
        ),
      ),
    );
  }
}
