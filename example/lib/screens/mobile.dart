// ignore_for_file: avoid_print

import 'package:fluent_ui/fluent_ui.dart';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart' show Icons;
//手机页面布局
class Mobile extends StatefulWidget {
  const Mobile({Key? key}) : super(key: key);

  @override
  _MobileState createState() => _MobileState();
}

class _MobileState extends State<Mobile> {
  int _currentIndex = 0;
  int _pillButtonBarIndex = 0;

  @override
  Widget build(BuildContext context) {
    // 使用框架页面
    return ScaffoldPage(
      //没有设置页面标题部分,再次嵌入了一组可由导航条控制的页面
      content: NavigationBody(
        children: [
          //第一页
          ScaffoldPage(
            header: const PageHeader(title: Text('Mobile')),
            // 开始页面的内容布局
            content: ListView(
              padding: EdgeInsets.symmetric(
                horizontal: PageHeader.horizontalPadding(context),
              ),
              children: [
                // 插入一行文字, 样式为子标题
                Text(
                  'Chips',
                  style: FluentTheme.of(context).typography.subtitle,
                ),
                //插入一个自动换行的流式布局
                Wrap(spacing: 10.0, runSpacing: 10.0, children: [
                  Chip(
                    image: const CircleAvatar(
                      radius: 12.0,
                      child: FlutterLogo(size: 14.0),
                    ),
                    text: const Text('Default'),
                    onPressed: () {
                      //点击后弹出底部选择对话框
                      showBottomSheet(
                        context: context,
                        isScrollControlled: true,
                        builder: (context) {
                          return BottomSheet(
                            // header: ListTile(
                            //   title: Text(
                            //     'Title',
                            //     style: FluentTheme.of(context)
                            //         .typography
                            //         .subtitle!
                            //         .copyWith(fontWeight: FontWeight.bold),
                            //   ),
                            //   trailing: Row(
                            //     children: List.generate(
                            //       6,
                            //       (_) => Padding(
                            //         padding: EdgeInsets.only(left: 24.0),
                            //         child: Icon(FluentIcons.circle_shape),
                            //       ),
                            //     ),
                            //   ),
                            // ),
                            description:
                                const Text('Description or Details here'),
                            children: [
                              const ListTile(
                                leading: Icon(FluentIcons.mail),
                                title: Text('Label'),
                                subtitle: Text('Label'),
                                trailing: Icon(FluentIcons.chevron_right),
                              ),
                              TappableListTile(
                                leading: const Icon(FluentIcons.mail),
                                title: const Text('Label'),
                                subtitle: const Text('Label'),
                                trailing: const Icon(FluentIcons.chevron_right),
                                onTap: () {
                                  print('tapped tile');
                                },
                              ),
                            ],
                          );
                        },
                      );
                    },
                  ),
                  const Chip(
                    image: CircleAvatar(
                      radius: 12.0,
                      child: FlutterLogo(size: 14.0),
                    ),
                    text: Text('Disabled'),
                    // Comment the onPressed function to disable the chip
                    // onPressed: () => print('pressed chip'),
                  ),
                  Chip.selected(
                    image: const CircleAvatar(
                      radius: 12.0,
                      child: FlutterLogo(size: 14.0),
                    ),
                    text: const Text('Active and selected'),
                    onPressed: () => print('pressed selected chip'),
                  ),
                  const Chip.selected(
                    image: CircleAvatar(
                      radius: 12.0,
                      child: FlutterLogo(size: 14.0),
                    ),
                    text: Text('Selected'),
                    // Comment the onPressed function to disable the chip
                    // onPressed: () => print('pressed chip'),
                  ),
                ]),
                //插入第二行文字
                Text(
                  'Snackbar',
                  style: FluentTheme.of(context).typography.subtitle,
                ),
                Wrap(runSpacing: 10.0, spacing: 10.0, children: [
                  //插入一个提示框
                  Snackbar(
                    content: const Text('Single-line snackbar'),
                    action: TextButton(
                      child: const Text('ACTION'),
                      // style: const ButtonThemeData(margin: EdgeInsets.zero),
                      onPressed: () {
                        //显示提示条
                        showSnackbar(
                          context,
                          const Snackbar(
                              content: Text('New update is available!')),
                        );
                      },
                    ),
                  ),
                  //插入一个多行提示框
                  Snackbar(
                    content: const Text(
                      'Multi-line snackbar block. Used when the content is too big',
                    ),
                    //启用多行文本扩展
                    extended: true,
                    action: TextButton(
                      child: const Text('ACTION'),
                      onPressed: () {
                        //显示提示条
                        showSnackbar(
                          context,
                          Snackbar(
                            content: const Text('New update is availble!'),
                            action: TextButton(
                              child: const Text('DOWNLOAD'),
                              onPressed: () {},
                            ),
                          ),
                        );
                      },
                    ),
                  )
                ]),
                //插入第三行文字
                Text(
                  'Other',
                  style: FluentTheme.of(context).typography.subtitle,
                ),
                //插入一个药丸按钮条，即一组标签按钮
                PillButtonBar(
                  selected: _pillButtonBarIndex,
                  onChanged: (i) => setState(() => _pillButtonBarIndex = i),
                  items: const [
                    PillButtonBarItem(text: Text('All')),
                    PillButtonBarItem(text: Text('Mail')),
                    PillButtonBarItem(text: Text('People')),
                    PillButtonBarItem(text: Text('Events')),
                  ],
                ),
              ],
            ),
          ),
          //第二页
          const ScaffoldPage(header: PageHeader(title: Text('Android'))),
          //第三页
          const ScaffoldPage(header: PageHeader(title: Text('iOS'))),
        ],
        index: _currentIndex,
      ),
      //设置横向的导航条
      bottomBar: BottomNavigation(
        index: _currentIndex,
        onChanged: (i) => setState(() => _currentIndex = i),
        items: const [
          BottomNavigationItem(
            icon: Icon(FluentIcons.split),
            title: Text('Both'),
          ),
          BottomNavigationItem(
            icon: Icon(Icons.phone_android_outlined),
            selectedIcon: Icon(Icons.phone_android),
            title: Text('Android'),
          ),
          BottomNavigationItem(
            icon: Icon(Icons.phone_iphone_outlined),
            selectedIcon: Icon(Icons.phone_iphone),
            title: Text('iOS'),
          ),
        ],
      ),
    );
  }
}
