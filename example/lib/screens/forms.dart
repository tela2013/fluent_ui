// ignore_for_file: avoid_print

import 'package:fluent_ui/fluent_ui.dart';
// 表单输入框元素的页面
class Forms extends StatefulWidget {
  const Forms({Key? key}) : super(key: key);

  @override
  _FormsState createState() => _FormsState();
}

class _FormsState extends State<Forms> {
  final autoSuggestBox = TextEditingController();

  final _clearController = TextEditingController();
  bool _showPassword = false;

  final values = ['Blue', 'Green', 'Yellow', 'Red'];
  String? comboBoxValue;

  DateTime date = DateTime.now();

  @override
  Widget build(BuildContext context) {
    // 内嵌导航面板的框架页
    return ScaffoldPage(
      // 设置当前页面的名称
      header: const PageHeader(title: Text('Forms showcase')),
      //插入一个列表视图
      content: ListView(
        //设置左右和底部的间距
        padding: EdgeInsets.only(
          bottom: kPageDefaultVerticalPadding,
          left: PageHeader.horizontalPadding(context),
          right: PageHeader.horizontalPadding(context),
        ),
        children: [
          // 设置列表中的元素

          // 插入一个文本框
          const TextBox(
            header: 'Email',
            placeholder: 'Type your email here :)',
          ),
          //插入行间距
          const SizedBox(height: 20),
          //插入一个行
          Row(children: [
            //插入一个弹性布局的文本框
            const Expanded(
              child: TextBox(
                //设置位只读框
                readOnly: true,
                placeholder: 'Read only text box',
              ),
            ),
            //设置列间距
            const SizedBox(width: 10),
            //插入一个弹性布局的文本框
            const Expanded(
              child: TextBox(
                //设置为禁用
                enabled: false,
                placeholder: 'Disabled text box',
              ),
            ),
            //设置列间距
            const SizedBox(width: 10),
            //插入一个弹性布局的复合框
            Expanded(
              //插入一个带下拉框选项的输入框
              child: AutoSuggestBox<String>(
                controller: autoSuggestBox,
                items: values,
                onSelected: (text) {
                  print(text);
                },
                textBoxBuilder: (context, controller, focusNode, key) {
                  //文本输入框
                  return TextBox(
                    key: key,
                    controller: controller,
                    focusNode: focusNode,
                    //设置文本框超出范围的焦点文字显示方式
                    suffixMode: OverlayVisibilityMode.editing,
                    suffix: IconButton(
                      icon: const Icon(FluentIcons.close),
                      onPressed: () {
                        controller.clear();
                        focusNode.unfocus();
                      },
                    ),
                    //设置占位符提示语
                    placeholder: 'Type a color',
                    clipBehavior:
                        focusNode.hasFocus ? Clip.none : Clip.antiAlias,
                  );
                },
              ),
            ),
          ]),
          const SizedBox(height: 20),
          //设置多行文本框
          TextBox(
            //不限制函数
            maxLines: null,
            controller: _clearController,
            //设置自动换行,总是显示焦点文字
            suffixMode: OverlayVisibilityMode.always,
            //设置最小高度
            minHeight: 100,
            //设置后缀图标
            suffix: IconButton(
              icon: const Icon(FluentIcons.close),
              onPressed: () {
                //点击后清除文本框
                _clearController.clear();
              },
            ),
            placeholder: 'Text box with clear button',
          ),
          const SizedBox(height: 20),
          //设置密码框
          TextBox(
            header: 'Password',
            placeholder: 'Type your placeholder here',
            //是否显示密码
            obscureText: !_showPassword,
            maxLines: 1,
            suffixMode: OverlayVisibilityMode.always,
            suffix: IconButton(
              //设置后缀图标的显示图案
              icon: Icon(
                !_showPassword ? FluentIcons.lock : FluentIcons.unlock,
              ),
              onPressed: () => setState(() => _showPassword = !_showPassword),
            ),
            //置于外部的后缀图标
            outsideSuffix: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: Button(
                child: const Text('Done'),
                onPressed: () {},
              ),
            ),
          ),
          const SizedBox(height: 20),

          //插入一个控件群组面板
          Mica(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Wrap(runSpacing: 8, children: [
                SizedBox(
                  width: 200,
                  //插入带标签名称的控件
                  child: InfoLabel(
                    label: 'Colors',
                    //插入一个选择框
                    child: Combobox<String>(
                      placeholder: const Text('Choose a color'),
                      //设置控件宽度是否自动扩展
                      isExpanded: true,
                      items: values
                          .map((e) => ComboboxItem<String>(
                                value: e,
                                child: Text(e),
                              ))
                          .toList(),
                      value: comboBoxValue,
                      onChanged: (value) {
                        print(value);
                        if (value != null) {
                          setState(() => comboBoxValue = value);
                        }
                      },
                    ),
                  ),
                ),
                //设置间距
                const SizedBox(width: 12),
                SizedBox(
                  width: 295,
                  //插入一个日期选择控件
                  child: DatePicker(
                    // popupHeight: kOneLineTileHeight * 6,
                    header: 'Date of birth',
                    selected: date,
                    onChanged: (v) => setState(() => date = v),
                  ),
                ),
                const SizedBox(width: 12),
                SizedBox(
                  width: 240,
                  //插入一个时间选择控件
                  child: TimePicker(
                    // popupHeight: kOneLineTileHeight * 5,
                    header: 'Arrival time',
                    selected: date,
                    onChanged: (v) => setState(() => date = v),
                  ),
                ),
              ]),
            ),
          ),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
